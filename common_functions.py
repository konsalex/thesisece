import numpy as np
import re
from scipy.spatial import distance as dist

CONFUSION_TEMPLATE = """
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 250px;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
</style>
</head>
<body>

<table>
<caption>Confusion Matrix</caption>
  <tr>
    <th></th>
    <th>Predicted Positive</th>
    <th>Predicted Negative</th>
  </tr>
  <tr>
    <th>Actual Positive</th>
    <td>TN=@TN</td>
    <td>FP=@FP</td>
  </tr>
  <tr>
    <th>Actual Negative</th>
    <td>FN=@FN</td>
    <td>TP=@TP</td>
  </tr>
 
</table>

</body>
</html>
"""


def get_camera_parameters(size):
    """
    Initializes the parameters of the camera.

    Args:
        size (tuple): size of the image 
    Returns:
        camera_matrix (np.array) : 3x3 cameraMatrix from the second link
        dist_coeffs (np.array) : 4x1 np.array of zeros
    Src:
        link : https://www.learnopencv.com/approximate-focal-length-for-webcams-and-cell-phone-cameras/
        link : https://www.learnopencv.com/head-pose-estimation-using-opencv-and-dlib/
    """
    focal_length = size[1]
    center = (size[1] / 2, size[0] / 2)
    camera_matrix = np.array([[focal_length, 0, center[0]],
                              [0, focal_length, center[1]], [0, 0, 1]],
                             dtype="double")
    dist_coeffs = np.zeros((4, 1))  # Assuming no lens distortion

    return camera_matrix, dist_coeffs


def get_full_image_points(landmarks):
    """
    Gets landmarks as a list and appends them as a tuple.

    Args:
        landmarks (list): [{x: int, y: int},...]
    Returns:
        image_points (np.array): [(x,y),(x,y),...] for the 68 points that dlibs provide
    """
    image_points = np.zeros((68, 2))

    for i in range(68):
        image_points[i, :] = (landmarks[i]['x'], landmarks[i]['y'])

    return image_points


def get_full_model_points(filename='model_points.txt'):
    """
    Read a file with points 68-3D model points

    Args:
        filename (string): name of the filename located in the same folder
    Returns:
        model_points (np.array): 68x3 [(x,y,x),(x,y,z)....]
    """
    raw_value = []
    with open(filename) as file:
        for line in file:
            raw_value.append(line)
    model_points = np.array(raw_value, dtype=np.float32)
    model_points = np.reshape(model_points, (3, -1)).T

    # Transform the model into a front view.
    model_points[:, 2] *= -1

    return model_points


def eye_aspect_ratio(eye):
    """
    Calculates the Eye Aspect Ratio from the points 36 to 42.

    Args:
        eye (list): landmarks[36:42]
    Returns:
        ear (float): A number showing the ratio of the Eyes
    Src: https://www.pyimagesearch.com/2017/04/24/eye-blink-detection-opencv-python-dlib/
    """
    # compute the euclidean distances between the two sets of
    # vertical eye landmarks (x, y)-coordinates
    A = dist.euclidean([eye[1]['x'], eye[1]['y']], [eye[5]['x'], eye[5]['y']])
    B = dist.euclidean([eye[2]['x'], eye[2]['y']], [eye[4]['x'], eye[4]['y']])

    # compute the euclidean distance between the horizontal
    # eye landmark (x, y)-coordinates
    C = dist.euclidean([eye[0]['x'], eye[0]['y']], [eye[3]['x'], eye[3]['y']])

    # compute the eye aspect ratio
    ear = (A + B) / (2.0 * C)

    # return the eye aspect ratio
    return ear


def generate_HTML_CM(confusion_matrix):
    """
    Generates an HTML representation of the confusion matrix

    Args:
        confusion_matrix (list): list of n (2,2) np.arrays containing individual confusion matrices
    Returns:
        html (string): HTML string with the table representation
    """

    tn, fp, fn, tp = np.mean(confusion_matrix, axis=0).ravel()
    replace_values = {
        "@TN": '%.2f' % tn,
        "@FP": '%.2f' % fp,
        "@FN": '%.2f' % fn,
        "@TP": '%.2f' % tp
    }

    replace_values = dict((re.escape(k), v) for k, v in replace_values.items())
    pattern = re.compile("|".join(replace_values.keys()))
    new_confusion_matrix = pattern.sub(
        lambda m: replace_values[re.escape(m.group(0))], CONFUSION_TEMPLATE)

    return new_confusion_matrix