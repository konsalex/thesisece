# Attention Prediction in a non-calibrated system

## Data Collection Program

### Info

The main idea is to show users two types of screens and ask them to hit Space to move one:

- A white screen with a black dot at a random point, where the user must look at the dot and hit Space
- A white screen with text instructing the user to look away from the screen and then hit Space

The key press triggers the webcam to capture a frame at that very moment and save it.

Participants have:

1. Their own UUID (identifier)
2. A yml file file containing their: Age, Gender, Laptop used, Screen Height, Screen Width
3. Folder containing Positive and Negative Examples

Each example has the following format `UUID_x_y.jpg` :
|Name|Value|
|---|---|
| UUID | The unique identifier of a participant |
| X | The X position of the displayed circle's center|
| Y | The Y position of the displayed circle's center|

### Example Screens

<figure>
    <img src="./Thesis Assets/Program Initialization.png"/>
    <figcaption style="text-align:center"> Example of the initialization screen
    </figcaption>
</figure>

<figure>
    <img style="border: 1px solid black;" src="./Thesis Assets/Not_Looking_Screen.png"/>
    <figcaption style="text-align:center"> Example of screen which directs participants to look out of the screen
    </figcaption>
</figure>

<figure>
    <img style="border: 1px solid black;" src="./Thesis Assets/Looking_at_screen.png"/>
    <figcaption style="text-align:center"> Example of screen which directs participants to look at the center of the circle
    </figcaption>
</figure>

<figure>
    <img src="./Thesis Assets/0e7047653b014264b33c1c3620e506f8_238_188.jpg"/>
    <figcaption style="text-align:center"> Example of a participant looking inside the screen
    </figcaption>
</figure>

<figure>
    <img src="./Thesis Assets/0e7047653b014264b33c1c3620e506f8_205_656.jpg"/>
    <figcaption style="text-align:center"> Example of a participant looking outside the screen
    </figcaption>
</figure>


## Analyses

Δούλεψα με τον εξής τρόπο στα δεδομένα. Πήρα για αρχή και δημιούργησα ένα Dataset από όλους τους συμμετέχοντες όπου είχε όλα τους τα landmarks per frame. Σαν landmarkds πήρα αυτά που δίνει η Dlib αλλά και την τοποθεσίας της κόρης μέσω των αλγορίθμων της Loceye.

<figure align="center">
    <img width=300 src="./Thesis Assets/DlibLandmarks.jpg"/>
    <figcaption style="text-align:center"> Landmarks given from Dlib
    </figcaption>
</figure>

Με τα outputs από τους αλγορίθμους της Loceye βρήκα το κέντρο της κόρης των participants ώστε να το χρησιμοποιήσω στην έρευνα μου καθώς δεν το δίνει η dlib.


| Participant Looking on the screen | Participant not looking on the screen |
|---|---|
| <img width=300 src="./Thesis Assets/PositiveExampleWithLandmarks.png"/>  |  <img width=300 src="./Thesis Assets/NegativeExampleWithLandmarks.png"/> |


### Τρόποι ανάλυσης/επίλυσης του προβλήματος


#### Leave one subject out - Dlib and PnP
> Running attention prediction for 54 participants and a total of 2728 images.
Each participant will be excluded from the training set once, our classifiers 
will be trained on the remaining participants and finally we will predict
the result for the one we left out. The total accuracy will be the mean value
of the accuracies of each participant

Έπειτα άρχισα την Leave one subject out ανάλυση. Άφηνα ενός δείγματος τα samples εκτός και έκανα train σε όλα τα υπόλοιπα και τέλος μετρούσα πόσα σωστά predictions έκανα για το συγκεκριμένο subject. Αυτό το έκανα 54 φορές για κάθε participant και σαν accuracy πήρα την μέση τιμή όλων των μεμονομένων accuracies.

Επίσης όσους έκαναν blink επειδή δίνανε θόρυβο τους διέγραφα από το training pool γιατί δίνανε θόρυβο στους classifiers (μπορεί να γίνει recommendation στο τέλος για further research).

Method : 

Papers: 
http://www.nada.kth.se/~sullivan/Papers/Kazemi_cvpr14.pdf (Dlib Paper + μήπως πρέπει να βάλω και paper Ελλήνων από Imperial)

Related Links:
1. http://dlib.net/face_landmark_detection_ex.cpp.html
2. https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
3. https://github.com/yinguobing/head-pose-estimation/tree/master/assets
4. https://github.com/TadasBaltrusaitis/OpenFace/blob/master/lib/local/LandmarkDetector/model/pdms/In-the-wild_aligned_PDM_68.txt (το 3D μοντέλο κεφαλιού)

Είναι ενδιαφέρον ότι το δείγμα μου είναι biased σε positive examples (~1500 positive, ~1100 negative), unbalanced.


#### Leave one subject out - 128 features and Dlib for pre-processing

Method : CNN - 128 feature extraction

Papers: https://cmusatyalab.github.io/openface/ , https://www.cv-foundation.org/openaccess/content_cvpr_2015/app/1A_089.pdf

Related Article : https://medium.com/@ageitgey/machine-learning-is-fun-part-4-modern-face-recognition-with-deep-learning-c3cffc121d78

Χρησιμοποίηση ένα pretrained CNN ώστε να μου παράξει 128 features όπου περιγράφουν το πρόσωπο του participant. (Στην ουσία τα 128 features είναι pre-trained από την OpenFace)

Έπειτα πάνω σε αυτά έκανα έναν classifier και δοκίμασα το accuracy.


#### Leave one subject out - FAN and PnP

Method : FAN-Face: a Simple Orthogonal Improvement to Deep Face Recognition

Paper used: https://www.adrianbulat.com/downloads/AAAI20/FANFace.pdf

Code : https://github.com/1adrianb/face-alignment

Χρησιμοποίησα κώδικα από αυτό το paper ώστε να παράξω σε 2D και 3D τα landmarks του προσώπου. Έπειτα έλυσα ξανά το PnP ώστε να πάρω Rotation και Translation vector και τα χρησιμοποίησα μαζί με τα 2D landmarks για να κάνω τον classifier μου. 

 142 features τα οποία θα τα κάνω train στους classifiers.

---
### Accuracy Table

|  Method | Avg. Accuracy (SVM) | Avg. Accuracy (RF) |
|---|---|---|
| Dlib with SolvePnP  |  0.6132697947214076 (με prob 0.3) | 0.6546920821114369 |
| CNN - 128 features  | 0.6606664113366526  | 0.5959402527767139 |
|  FAN-Face with SolvePnP |  0.6599004212945232 | 0.5959402527767139 |
| Dlib and Iris Vector with SolvePnP  |  0.750733137829912 |0.7800586510263929 |
 
---

#### [Perspective-n-Point](https://en.wikipedia.org/wiki/Perspective-n-Point)


Στην αρχή δούλεψα στο predictions.py μόνο με την Dlib και solvePnP δεδομένα προσπαθώντας να καταλάβω ποια επιπλέον features θα μου ανεβάσουν το accuracy του classifier και τι θα έπρεπε να προσέξω από θόρυβο. Επίσης δεν ήταν σωστή ανάλυση καθώς το train και το test pool μπορεί να είχαν εικόνες από τον ίδιο participant αλλά μου έδινε μία πρώτη εικόνα.

> Από τα δεδομένα/features που έπαιρνα από την κάθε μέθοδο έκανα έπειτα έναν classifier και δοκίμασα SVM, Logistic Regression και Random Forest Classifier ώστε να δω ποιος αποδίδει καλύτερα σε αυτό το πρόβλημα.

> Σε όλα είχα test size 20% του συνολικού dataset καθώς ήταν αρκετά μικρό.

**Iteration 1:**

In this data architecture we will use as input the translation vector and rotation vector, each one of shape (3, 1) so a signle training example of our dataset will be of shape (6, 1)

<figure align="center">
    <img width=300 src="./Thesis Assets/PredictionPyFirstRun.png"/>
    <figcaption style="text-align:center"> Αποτελέσματα Πρώτης προσπάθειας (με επικάλυψη των subjects)
    </figcaption>
</figure>

**Iteration 2:**

In this data architecture we will use as input the translation vector and rotation vector plus the 2 iris points, so a signle training example of our dataset will be of shape (10, 1)

<figure align="center">
    <img width=300 src="./Thesis Assets/PredictionPySecondRun.png"/>
    <figcaption style="text-align:center"> Αποτελέσματα Δεύτερης προσπάθειας (με επικάλυψη των subjects)
    </figcaption>
</figure>


**Iteration 3:**

In this archtecture we will use the difference vector between the iris and the inner edge point of the eye. First, we will keep the raw iris coordinates and add the 2 vectors to our training data, yielding to a shape of (14, 1).

<figure align="center">
    <img width=300 src="./Thesis Assets/EyesLandmark.png"/>
    <figcaption style="text-align:center"> Τα σημεία που δώσανε το vector μεταξύ της κόρης και της βλεφαρικής σχισμής.
    </figcaption>
</figure>

<figure align="center">
    <img width=300 src="./Thesis Assets/PredictionPyThirdRun.png"/>
    <figcaption style="text-align:center"> Αποτελέσματα Τρίτης προσπάθειας (με επικάλυψη των subjects)
    </figcaption>
</figure>

---

## Dlib with PnP and Facial Landmarks

Features (142,1)

<figure align="center">
    <img width=300 src="./Thesis Assets/Dlib Analyses/WithoutEyes.png"/>
    <figcaption style="text-align:center"> Αποτελέσματα Πρώτης προσπάθειας (με επικάλυψη των subjects)
    </figcaption>
</figure>

Features (146,1)

<figure align="center">
    <img width=300 src="./Thesis Assets/Dlib Analyses/WithEyesOnly.png"/>
    <figcaption style="text-align:center"> Αποτελέσματα Πρώτης προσπάθειας (με επικάλυψη των subjects)
    </figcaption>
</figure>

Features (150,1)

<figure align="center">
    <img width=300 src="./Thesis Assets/Dlib Analyses/WithEyesAndDiffVector.png"/>
    <figcaption style="text-align:center"> Αποτελέσματα Πρώτης προσπάθειας (με επικάλυψη των subjects)
    </figcaption>
</figure>