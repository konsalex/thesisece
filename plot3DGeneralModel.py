import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from common_functions import get_full_model_points

model = get_full_model_points()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')


for p in model:
	ax.scatter(p[0], p[1], p[2], marker='o', color="r")

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()
