import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D

dtStorage = pd.read_pickle("./FAN_data.pkl")
random = 420

# Get First Head on the list
minCoord = np.amin(dtStorage.iloc[random]['3d'][0][:, 2])
maxCoord = np.amax(dtStorage.iloc[random]['3d'][0][:, 2])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
im = cv2.imread(dtStorage.iloc[random]['filename'])
for mark in dtStorage.iloc[random]['2d'][0]:
    cv2.circle(im,(mark[0],mark[1]),2, (0,20,255),-1)

cv2.imwrite("./exampleFAN2dLandmarks.jpg", im)

ax.plot_trisurf(dtStorage.iloc[random]['3d'][0][:,0], dtStorage.iloc[random]['3d'][0][:,1], dtStorage.iloc[random]['3d'][0][:,2], antialiased=True, cmap =  plt.get_cmap('gist_heat'))

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()

test = input("How does it look like")
